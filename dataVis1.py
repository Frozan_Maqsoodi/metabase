import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import lfilter, butter, filtfilt
import scipy.signal as signal

data = pd.read_csv('BN_2018_08_07_10_06_35.csv')
data1= data[(data.type == 1) & (data.model =='E3:70:62:A8:5E:7C')]
# data2= data[(data.type == 1) & (data.model =='E3:70:62:A8:5E:7C')]

# First, design the Buterworth filter
N = 6  # Filter order
fc = 20/1000  # Cutoff frequency

# Second, apply the filter
b,a = butter(N, fc)

tempf = filtfilt(b,a,data1.w)
plt.figure()
# ax1 = plt.subplot(311)
# plt.title('Linear Acceleration w')
# data1.groupby('model')['w'].plot(kind='line')
# plt.legend(loc="best")
#
# ax2 = plt.subplot(312)
# plt.title('Linear Acceleration x')
# data1.groupby('model')['x'].plot(kind='line')
#
# ax3 = plt.subplot(313)
# plt.title('Linear Acceleration y')
# data1.groupby('model')['y'].plot(kind='line')

data1['w'].plot(kind='line')
plt.title('Filtered  w')
plt.plot(data1.index,tempf)


plt.show()
