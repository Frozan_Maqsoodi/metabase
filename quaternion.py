import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import lfilter, butter, filtfilt
import scipy.signal as signal

data = pd.read_csv('BN_2018_08_07_10_06_35.csv')
data1= data[(data.type == 0) & (data.model =='E3:70:62:A8:5E:7C')]
data2= data[(data.type == 0)& (data.model =='D3:E2:18:D2:2C:9B') ]

# First, design the Buterworth filter
N = 6  # Filter order
fc = 20/1000  # Cutoff frequency

# Second, apply the filter
b,a = butter(N, fc)
# filter left hand sensor
temp_w = filtfilt(b, a, data1.w)
temp_x = filtfilt(b, a, data1.x)
temp_y = filtfilt(b, a, data1.y)
temp_z = filtfilt(b, a, data1.z)
# filter right hand sensor
temp_w1 = filtfilt(b, a, data2.w)
temp_x1 = filtfilt(b, a, data2.x)
temp_y1 = filtfilt(b, a, data2.y)
temp_z1 = filtfilt(b, a, data2.z)

plt.figure()
ax1 = plt.subplot(411)
plt.title('Quaternion w')
# data1.groupby('model')['w'].plot(kind='line')
plt.legend(loc="best")
plt.plot(data1.index, temp_w)
plt.plot(data2.index, temp_w1)

ax2 = plt.subplot(412)
plt.title('Quaternion x')
# data1.groupby('model')['x'].plot(kind='line')
plt.plot(data1.index, temp_x)
plt.plot(data2.index, temp_x1)

ax3 = plt.subplot(413)
plt.title('Quaternion y')
# data1.groupby('model')['y'].plot(kind='line')
plt.plot(data1.index, temp_y)
plt.plot(data2.index, temp_y1)

ax4 = plt.subplot(414)
plt.title('Quaternion z')
plt.xlabel('Time in ms')
plt.ylabel('Amplitude')
# data1.groupby('model')['y'].plot(kind='line')
plt.plot(data1.index, temp_z)
plt.plot(data2.index, temp_z1)


plt.show()
