# coding: utf-8

# # Analyse MetaSensor data of a stroke patient

__author__ = 'Frozan Maqsoodi <frozan.maqsoodi@callaghaninnovation.govt.nz>'
__ver__ = '0.1.0'
__date__ = '2018-07-28'

import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt
import numpy as np

headers = ['time', 'model', 'type', 'count', 'w', 'x', 'y', 'z']
data = pd.read_csv('data/alldata.csv', names=headers)

# filter the right hand and left hand sensor and accelerometer
RH_data = data[(data.type == 1) & (data.model == 'D3:E2:18:D2:2C:9B')]
LH_data = data[(data.type == 1) & (data.model == 'E3:70:62:A8:5E:7C')]

# read the Torso sensor
Torso = data[(data.type == 1) & (data.model == 'E3:68:99:25:5F:85')]


# converting the unix timestamp to pandas datetime format
RH_data.index = pd.to_datetime(RH_data['time'], unit='ms', infer_datetime_format=True)
LH_data.index = pd.to_datetime(LH_data['time'], unit='ms', infer_datetime_format=True)
# LH_data.index = LH_data.index.tz_localize('utc').tz_convert('Pacific/Auckland')
# RH_data.index = RH_data.index.tz_localize('utc').tz_convert('Pacific/Auckland')


# filtering the data to accelerometers vectors only
RH_data = RH_data[['w', 'x', 'y']]
LH_data = LH_data[['w', 'x', 'y']]

x = RH_data.w
y = RH_data.x
z = RH_data.y

left_x = LH_data.w
left_y = LH_data.x
left_z = LH_data.y

# First, design the Butter-worth filter
N = 1  # Filter order
fc = 1/50  # Cutoff frequency
# Second, apply the filter
b, a = butter(N, fc, btype='low')


left_low_x = filtfilt(b, a, left_x)
left_low_y = filtfilt(b, a, left_y)
left_low_z = filtfilt(b, a, left_z)

low_x = filtfilt(b, a, x)
low_y = filtfilt(b, a, y)
low_z = filtfilt(b, a, z)

# convert to m/s^2
low_x *= 9.81
low_y *= 9.81
low_z *= 9.81

left_low_x *= 9.81
left_low_y *= 9.81
left_low_z *= 9.81

x_sq = np.square(low_x)
y_sq = np.square(low_y)
z_sq = np.square(low_z)

left_x_sq = np.square(left_low_x)
left_y_sq = np.square(left_low_y)
left_z_sq = np.square(left_low_z)

abs = np.sqrt(x_sq+y_sq+z_sq)-9.81
left_abs = np.sqrt(left_x_sq + left_y_sq + left_z_sq) - 9.81

# design high pass filter
fc = .3/50  # Cutoff frequency
# Second, apply the filter
b, a = butter(N, fc, btype='high')
high_abs = filtfilt(b, a, abs)
left_high_abs = filtfilt(b, a, left_abs)
#
plt.figure()
plt.title('Filtered left and right hand signal')
plt.plot(RH_data.index,high_abs)
plt.plot(LH_data.index,left_high_abs)
plt.legend(('RH','LH'))
plt.xlabel(('time plot from 10:08 am to 2:38 pm'))
plt.ylabel('amplitude')
plt.grid()

# convert to pandas data-frame
df = pd.Series(high_abs, index=RH_data.index)
df1 = pd.Series(left_high_abs, index=LH_data.index)
df.index = df.index.tz_localize('utc').tz_convert('Pacific/Auckland')
# Find the running total of right hand to 1 minute & resample to 1 minute or any other value
RH_data_rolling = df.abs().rolling('1min').sum()
RH_data_resample = df.abs().resample('1min').sum()

# Find the running total of left hand to 1 minute & resample to 1 minute or any other value
LH_data_rolling = df1.abs().rolling('1min').sum()
LH_data_resample = df1.abs().resample('1min').sum()

# Plot the left and right hand running sum
plt.figure()
plt.title('Rolling 1 minute RH and LH signal')
plt.plot(RH_data_rolling)
plt.plot(LH_data_rolling)
plt.legend(('RH','LH'))
plt.xlabel(('time plot from 10:08 am to 2:38 pm'))
plt.ylabel('rolling sum of the magnitude')
plt.grid()

plt.figure()
plt.title('Re-sampled 1 min RH and LH signal')
plt.plot(RH_data_resample, marker='o')
plt.plot(LH_data_resample, marker='o')
plt.xlabel(('time plot from 10:08 am to 2:38 pm'))
plt.ylabel('resampling sum of the magnitude')
plt.legend(('RH','LH'))
plt.grid()
plt.show()
