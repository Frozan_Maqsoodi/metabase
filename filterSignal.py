import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt

data = pd.read_csv('BN_2018_08_07_10_06_35.csv')
data1= data[(data.type == 1)& (data.model =='E3:70:62:A8:5E:7C') ]
data2= data[(data.type == 1)& (data.model =='D3:E2:18:D2:2C:9B') ]

# First, design the Buterworth filter
N = 6  # Filter order
fc = 20/1000  # Cutoff frequency

# Second, apply the filter
b,a = butter(N, fc)

temp_w = filtfilt(b, a, data1.w)
temp_x = filtfilt(b, a, data1.x)
temp_y = filtfilt(b, a, data1.y)

temp_w2 = filtfilt(b, a, data2.w)
temp_x2 = filtfilt(b, a, data2.x)
temp_y2= filtfilt(b, a, data2.y)

plt.figure()
# Plot left and right hand filtered signals
ax1 = plt.subplot(411)
plt.title('Linear Acceleration  w')
plt.plot(data1.index, temp_w)
plt.plot(data2.index, temp_w2)


ax2 = plt.subplot(412)
# data1['x'].plot(kind='line')
plt.title('Linear Acceleration  x')
plt.plot(data1.index, temp_x)
plt.plot(data2.index, temp_x2)

ax3 = plt.subplot(413)
# data1['y'].plot(kind='line')
plt.title('Linear Acceleration  y')
# plt.xlabel('time in ms')
# plt.ylabel('Amplitude')
plt.plot(data1.index, temp_y)
plt.plot(data2.index, temp_y2)

ax3 = plt.subplot(414)
# data1['y'].plot(kind='line')
plt.title('Linear Acceleration ')
plt.xlabel('time in ms')
plt.ylabel('Amplitude')
plt.legend(loc='best')
plt.plot(data1.index, temp_w)
plt.plot(data1.index, temp_x)
plt.plot(data1.index, temp_y)
plt.plot(data2.index, temp_w2)
plt.plot(data2.index, temp_x2)
plt.plot(data2.index, temp_y2)

# Plot left hand filtered and unfiltered signals
# ax2 = plt.subplot(311)
# data1['w'].plot(kind='line')
# plt.title('Filtered and Unfiltered w')
# plt.plot(data1.index, temp_w)

# ax2 = plt.subplot(312)
# data1['x'].plot(kind='line')
# plt.title('Filtered and Unfiltered x')
# plt.plot(data1.index, temp_x)
#
# ax2 = plt.subplot(313)
# plt.xlabel('time in ms')
# plt.ylabel('Amplitude')
# data1['y'].plot(kind='line')
# plt.title('Filtered and Unfiltered y')
# plt.plot(data1.index, temp_y)
plt.show()
