This project uses accelerometer data for the analysis of left and right hand movement.
The accelerometer.py computes a rolling sum and resampling at every 5 and 1 minute.

Before running the script, make sure you have the following tools installed properly
- python 3
- pandas
- matplotlib
- scipy
- numpy

To run the script, open a shell command or window's terminal and type
python accelerometer.py