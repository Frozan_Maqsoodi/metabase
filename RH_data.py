import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt
import numpy as np
data = pd.read_csv('data/alldata.csv')
RH_data = data[(data.type == 1) & (data.model == 'D3:E2:18:D2:2C:9B')]
RH_data.index = pd.to_datetime(RH_data['time'], unit='ms', infer_datetime_format=True)
RH_data = RH_data[['w', 'x', 'y']]

x = RH_data.w
y = RH_data.x
z = RH_data.y

# First, design the Butter-worth filter
N = 1  # Filter order
fc = 1/50  # Cutoff frequency
# Second, apply the filter
b, a = butter(N, fc, btype='low')

low_x = filtfilt(b, a, x)
low_y = filtfilt(b, a, y)
low_z = filtfilt(b, a, z)

# convert to m/s^2
low_x *= 9.81
low_y *= 9.81
low_z *= 9.81
x_sq = np.square(low_x)
y_sq = np.square(low_y)
z_sq = np.square(low_z)

abs = np.sqrt(x_sq+y_sq+z_sq)-9.81

# design high pass filter
fc = .3/50  # Cutoff frequency
# Second, apply the filter
b, a = butter(N, fc, btype='high')
high_abs = filtfilt(b, a, abs)

# convert to pandas data-frame
df = pd.Series(high_abs, index=RH_data.index)

LH_data_rolling = df.abs().rolling('5min').sum()
LH_data_resample = df.abs().resample('1min').mean()

plt.figure()
plt.title('Rolling RH signal')
plt.plot(LH_data_rolling)
plt.grid()
plt.figure()
plt.title('Re-sampled 1min RH signal')
plt.plot(LH_data_resample, marker='o')
plt.grid()
plt.show()
