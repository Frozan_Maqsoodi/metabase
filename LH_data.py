import pandas as pd
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt
import numpy as np

# read the data
data = pd.read_csv('data/alldata.csv')

# filtering data to linear acceleration only and sensor attached to left hand
LH_data = data[(data.type == 1) & (data.model =='E3:70:62:A8:5E:7C')]

# converting the unix timestamp to pandas datetime format
LH_data.index = pd.to_datetime(LH_data['time'], unit='ms')

# filtering the data to accelerometers vectors only
LH_data = LH_data[['w', 'x', 'y']]

# store the accelerometers vectors to x, y, z
x = LH_data.w
y = LH_data.x
z = LH_data.y

# First, design the Butter-worth filter for low pass filter
N = 1  # Filter order
fc = 1/50  # Cutoff frequency
b, a = butter(N, fc, btype='low')

# Second, apply the filter
low_x = filtfilt(b, a, x)
low_y = filtfilt(b, a, y)
low_z = filtfilt(b, a, z)

# convert to m/s^2
low_x *= 9.81
low_y *= 9.81
low_z *= 9.81

# taking the magnitude of accelerometer vectors
abs = np.sqrt(low_x**2+low_y**2+low_z**2)

# design high pass filter
fc = .3/50  # Cutoff frequency
# Second, apply the filter
b, a = butter(N, fc, btype='high')
high_abs = filtfilt(b, a, abs)

# convert to pandas data-frame
df = pd.Series(high_abs, index=LH_data.index)
# do a rolling mean or sum of signal data by setting the window size to desired value
LH_data_rolling = df.abs().rolling('5min').sum()

# resample data based on the window size
LH_data_resample = df.abs().resample('1min').mean()

# plt.title('Absolute LH filtered data')
# plt.plot(LH_data.index, high_abs)
# plt.grid()
plt.figure()
plt.title('Rolling LH signal')
plt.plot(LH_data_rolling)
plt.grid()
plt.figure()
plt.title('Re-sampled 1min LH signal')
plt.plot(LH_data_resample, marker='o')
plt.grid()
plt.show()
